package com.example.androidtraining

import android.app.Application
import com.example.androidtraining.model.DataItemRecyclerService

class App: Application() {

    val dataItemRecyclerService = DataItemRecyclerService()
}