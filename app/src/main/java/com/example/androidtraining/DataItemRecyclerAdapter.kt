package com.example.androidtraining

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtraining.databinding.ItemRecyclerBinding
import com.example.androidtraining.model.DataItemRecycler

interface ItemActionListener {
    fun onItemDetails(dataItemRecycler: DataItemRecycler)
}

class DataItemRecyclerAdapter(
    private  val actionListener: ItemActionListener
): RecyclerView.Adapter<DataItemRecyclerAdapter.DataItemRecyclerViewHolder>(), View.OnClickListener {

    class DataItemRecyclerViewHolder(
        val binding: ItemRecyclerBinding
    ): RecyclerView.ViewHolder(binding.root)

    override fun onClick(v: View?) {
        val item = v?.tag as DataItemRecycler
        actionListener.onItemDetails(item)
    }

    var items : List<DataItemRecycler> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataItemRecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRecyclerBinding.inflate(inflater, parent, false)

        binding.root.setOnClickListener(this)

        return DataItemRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DataItemRecyclerViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.tag = item
        with(holder.binding) {
            title.text = item.title
            description.text = item.description

        }
    }

    override fun getItemCount(): Int = items.size



}
