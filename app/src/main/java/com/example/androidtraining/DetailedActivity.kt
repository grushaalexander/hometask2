package com.example.androidtraining

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtraining.databinding.ActivityDetailedBinding
import com.example.androidtraining.databinding.ActivityMainBinding

class DetailedActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailedBinding

    companion object{
        const val TITLE = "title"
        const val DESCRIPTION = "description"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.titleTextView.text = intent.getStringExtra(TITLE)
        binding.descriptionTextView.text = intent.getStringExtra(DESCRIPTION)
    }
}