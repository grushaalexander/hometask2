package com.example.androidtraining

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.androidtraining.databinding.ActivityMainBinding
import com.example.androidtraining.model.DataItemRecycler
import com.example.androidtraining.model.DataItemRecyclerService
import com.example.androidtraining.model.ItemListeners

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: DataItemRecyclerAdapter

    private  val dataItemRecyclerService: DataItemRecyclerService
        get() = (applicationContext as App).dataItemRecyclerService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = DataItemRecyclerAdapter(object: ItemActionListener{
            override fun onItemDetails(dataItemRecycler: DataItemRecycler){
                start(dataItemRecycler)
            }
        })

        val linearLayoutManager = LinearLayoutManager(this)
        //linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.adapter = adapter

        dataItemRecyclerService.addListener(itemListeners)

        binding.recyclerView.setOnClickListener {
        }
    }

    fun start(dataItemRecycler: DataItemRecycler){
        intent = Intent(
            this,
            DetailedActivity::class.java
        )
        intent.putExtra(DetailedActivity.TITLE, dataItemRecycler.title)
        intent.putExtra(DetailedActivity.DESCRIPTION, dataItemRecycler.description )
        startActivity(intent)
    }

    private val itemListeners: ItemListeners = {
        adapter.items = it
    }
}