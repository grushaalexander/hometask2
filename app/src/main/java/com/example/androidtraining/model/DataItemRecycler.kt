package com.example.androidtraining.model

data class DataItemRecycler(
    val title: String,
    val description: String
)
