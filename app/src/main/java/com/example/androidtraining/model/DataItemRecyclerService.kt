package com.example.androidtraining.model

typealias ItemListeners = (items: List<DataItemRecycler>) -> Unit

class DataItemRecyclerService {

    private var items = mutableListOf<DataItemRecycler>()

    private val listeners = mutableSetOf<ItemListeners>()

    init {
        items = (1..1000).map {
            DataItemRecycler(
                title = "Title $it",
                description = "Description $it"
            )
        }.toMutableList()
    }

    fun addListener(listener: ItemListeners){
        listeners.add(listener)
        listener.invoke(items)
    }
    fun removeListener(listener: ItemListeners){
        listeners.remove(listener)
    }
    private fun notifyChanges(){
        listeners.forEach{it.invoke(items)}
    }
}
